package service;

import model.*;

import java.util.Scanner;

public class Order {

    public void makeAnOrder() {

        int id = 0;
        int choice = 0;

        System.out.println("Customer info");
        System.out.println("Please enter first name:");
        Scanner fn = new Scanner(System.in);
        String fname = fn.nextLine();
        System.out.println("Please enter last name:");
        Scanner ln = new Scanner(System.in);
        String lname = ln.nextLine();

        do {
            System.out.println("Make an order. \n\r1: for Shoe" + "\n\r2: for Cloth" + "\n\r3: for Accessory" + "\n\r4: Exit");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            id++;
            switch (choice) {
                case 1:

                    Shoe shoe = new Shoe();

                    System.out.println("Please enter shoe name:");
                    Scanner name = new Scanner(System.in);
                    shoe.setName(name.nextLine());
                    System.out.println("Please enter price:");
                    Scanner price = new Scanner(System.in);
                    shoe.setPrice(price.nextDouble());
                    System.out.println("Please enter producer name:");
                    Scanner prodName = new Scanner(System.in);
                    shoe.setProducerName(prodName.nextLine());
                    System.out.println("Please enter description:");
                    Scanner description = new Scanner(System.in);
                    shoe.setDescription(description.nextLine());
                    System.out.println("Please enter quantity:");
                    Scanner quantity = new Scanner(System.in);
                    shoe.setQuantity(quantity.nextInt());
                    System.out.println("Please enter size:");
                    Scanner size = new Scanner(System.in);
                    shoe.setShoeSize(size.nextInt());
                    System.out.println("Please enter color:");
                    Scanner color = new Scanner(System.in);
                    shoe.setColor(color.nextLine());
                    System.out.println("Please enter men or women:");
                    Scanner category = new Scanner(System.in);
                    shoe.setCategory(category.nextLine());

                    System.out.println("Customer:"+ fname +" "+lname);
                    System.out.println("Product item #"+id+": shoe, "+shoe.getName()+", "+shoe.getPrice()+"€, "+shoe.getProducerName()+", "+shoe.getDescription()+", "+shoe.getShoeSize()+", "+shoe.getColor()+", "+shoe.getCategory()+", "+shoe.getQuantity());

                    break;

                case 2:

                    Cloth cloth = new Cloth();

                    System.out.println("Enter cloth name:");
                    Scanner cName = new Scanner(System.in);
                    cloth.setName(cName.nextLine());
                    System.out.println("Please enter price:");
                    Scanner cPrice = new Scanner(System.in);
                    cloth.setPrice(cPrice.nextDouble());
                    System.out.println("Please enter producer name:");
                    Scanner cProdName = new Scanner(System.in);
                    cloth.setProducerName(cProdName.nextLine());
                    System.out.println("Please enter description:");
                    Scanner cDescription = new Scanner(System.in);
                    cloth.setDescription(cDescription.nextLine());
                    System.out.println("Please enter quantity:");
                    Scanner cQuantity = new Scanner(System.in);
                    cloth.setQuantity(cQuantity.nextInt());
                    System.out.println("Please enter cloth size:");
                    Scanner clothSize = new Scanner(System.in);
                    cloth.setClothSize(clothSize.nextLine());
                    System.out.println("Please enter collection name:");
                    Scanner collectionName = new Scanner(System.in);
                    cloth.setCollectionName(collectionName.nextLine());

                    System.out.println("Customer:"+ fname +" "+lname);
                    System.out.println("Product item #"+id+": shoe, "+cloth.getName()+", "+cloth.getPrice()+"€, "+cloth.getProducerName()+", "+cloth.getDescription()+", "+cloth.getClothSize()+", "+cloth.getCollectionName()+", "+cloth.getQuantity());


                    break;

                case 3:

                    Accessory accessory = new Accessory();

                    System.out.println("Enter accessory name:");
                    Scanner aName = new Scanner(System.in);
                    accessory.setName(aName.nextLine());
                    System.out.println("Please enter price:");
                    Scanner aPrice = new Scanner(System.in);
                    accessory.setPrice(aPrice.nextDouble());
                    System.out.println("Please enter producer name:");
                    Scanner aProdName = new Scanner(System.in);
                    accessory.setProducerName(aProdName.nextLine());
                    System.out.println("Please enter description:");
                    Scanner aDescription = new Scanner(System.in);
                    accessory.setDescription(aDescription.nextLine());
                    System.out.println("Please enter quantity:");
                    Scanner aQuantity = new Scanner(System.in);
                    accessory.setQuantity(aQuantity.nextInt());
                    System.out.println("Enter accessory material:");
                    Scanner aMaterial = new Scanner(System.in);
                    accessory.setMaterial(aName.nextLine());
                    System.out.println("Please enter weight:");
                    Scanner aWeight = new Scanner(System.in);
                    accessory.setAvgWeight(aWeight.nextDouble());

                    System.out.println("Customer:"+ fname +" "+lname);
                    System.out.println("Product item #"+id+": shoe, "+accessory.getName()+", "+accessory.getPrice()+"€, "+accessory.getProducerName()+", "+accessory.getDescription()+", "+accessory.getMaterial()+", "+accessory.getAvgWeight()+", "+accessory.getQuantity());


                    break;

                case 4:
                    System.out.println("Exiting.....");
                    break;

                default:
                    break;
            }
        }while (choice >= 1 || choice <=4);
    }
}
