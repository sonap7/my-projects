import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CalcStats extends HeartRates {

    public void calculate(){
        long age=0;
        int hrMax=0;
        int hrTar=0;

        System.out.println("Please enter first name:");
        Scanner fn = new Scanner(System.in);
        setfName(fn.nextLine());

        System.out.println("Please enter last name:");
        Scanner ln = new Scanner(System.in);
        setlName(ln.nextLine());

        System.out.println("Enter day of birth: ");
        Scanner day = new Scanner(System.in);
        setDay(day.nextInt());

        System.out.println("Enter month of birth: ");
        Scanner mn = new Scanner(System.in);
        setMonth(mn.nextInt());

        System.out.println("Enter year of birth: ");
        Scanner yr = new Scanner(System.in);
        setYear(yr.nextInt());

        System.out.println("Please enter last name:");
        Scanner fnOut = new Scanner(System.in);
        String fileName = fnOut.nextLine();

        LocalDate date = LocalDate.of(getYear(), getMonth() ,getDay());
        LocalDate now = LocalDate.now();

        age = ChronoUnit.YEARS.between(date, now);
        hrMax = 220-(int)age;
        hrTar = (85*hrMax)/100;

        String monthString;
        switch (getMonth()) {
            case 1:  monthString = "January";       break;
            case 2:  monthString = "February";      break;
            case 3:  monthString = "March";         break;
            case 4:  monthString = "April";         break;
            case 5:  monthString = "May";           break;
            case 6:  monthString = "June";          break;
            case 7:  monthString = "July";          break;
            case 8:  monthString = "August";        break;
            case 9:  monthString = "September";     break;
            case 10: monthString = "October";       break;
            case 11: monthString = "November";      break;
            case 12: monthString = "December";      break;
            default: monthString = "Invalid month"; break;
        }

        PrintWriter pw;
        try{
            pw = new PrintWriter("HeartRateStats.txt");
            pw.println("\nFirst Name: "+getfName() +
                    "\nLast Name: "+ getlName() +
                    "\nDay of Birth: "+ getDay() +
                    "\nMonth of birth: "+ monthString +
                    "\nYear of Birth "+ getYear() +
                    "\nAge is: "+ age +
                    "\nMax HeartRate is: "+ hrMax +
                    "\nTarget HeartRate is: "+ hrTar);
            pw.println("\n\rEnd of file");
            pw.close();
        } catch (FileNotFoundException e){
            System.out.println("Error Occured!");
        }

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("HeartRates Calculation");
        Object[][] data = {
                {"First Name", getfName()},
                {"Last Name", getlName()},
                {"Day of Birth", getDay()},
                {"Month of Birth", monthString},
                {"Year of Birth", getYear()},
                {"Age", (int)age},
                {"Max HeartRate", hrMax},
                {"Target HeartRate", hrTar}
        };

        int rowNum = 0;
        System.out.println("Creating excel");

        for (Object[] datatype : data) {
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : datatype) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done");
    }

}
