import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        CalcStats HRStatsIO = new CalcStats();
        HRStatsIO.calculate();

        File file = new File("HeartRateStats.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        while((st = br.readLine()) != null)
            System.out.println(st);
    }
}
