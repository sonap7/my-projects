package model;

public class Employee {
    private String firstName;
    private String lastName;
    private double salary;
    private String type;
    private String address;

    public Employee() {
    }

    public Employee(String firstName, String lastName, double salary, String type, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.type = type;
        this.address = address;
    }

    public void calcSalary(){
        if (type == "Manager"){
            salary = salary + ((4*salary)/100);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return  "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", salary=" + salary +
                ", type='" + type + '\'' +
                ", address='" + address + '\'' +
                '}'+"\n\r";
    }
}
