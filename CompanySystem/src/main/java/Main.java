import model.Employee;
import service.CompanyImpl;
import service.InitEmployee;

public class Main {
    public static void main(String[] args) {

        CompanyImpl company = new CompanyImpl();
        company.simpleStats(InitEmployee.employeeList());

    }
}
