package gr.example.ebookstore.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private long id;

    @Column(nullable = false)
    private int numberOfPages;

    @Column(unique = true, nullable = false)
    private String title;

    @Column(nullable = false)
    private String author;
}
